# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.0.0] - 2017-06-20
### Added
 - New visual identity by @tylerfortune8.

## 0.0.1 - 2018-07-16
### Added
 - Change Separador de Decimales from ',' to '.' (Ajustes->Idiomas->Spanish(PE))
 - Change Digitos de Product Unit of Measure de 3 a 0 (Ajustes->Estructura de la base de datos->Presicion decimal->Product Unit of Mesure)
 - Hide Debates (Ajustes->Interfaz de Usuario->Elementos de Menu->Debates->Archivar)
 - Hide Ajustes/Tablero (Ajustes->Interfaz de Usuario->Elementos de Menu->Ajustes/Tablero->Archivar)
 - Hide Aplicaciones (Ajustes->Interfaz de Usuario->Elementos de Menu->Aplicaciones->Archivar)
 - Edit Login and translate (Ajustes->Interfaz de Usuario->Vistas->Login)
 - Edit Login Layout and hide databases (Ajustes->Interfaz de Usuario->Vistas->Login Layout)
### Changed
### Deprecated
### Removed
### Fixed
### Security