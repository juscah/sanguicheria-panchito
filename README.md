# PANCHITO SOFTWARE

## PRIVATE DATA

### BaseData
```

DatabaseName: sangucheriapanchito
email: admin@gmail.com
password: qweRTY123


# PGADMIN
# usuario y password estan en el docker-compose.yml
docker run -p 5050:5050 --link panchito_db_1:panchito_db_1 --network panchito_default --rm nphung/pgadmin

# START
cd release
docker-compose up -d

# DELETE
docker-compose down
docker volume prune
```

#### Configurar Impresora
 - margin: minimum
 - scale: 90

#### Administrate database
/web/database/manager

#### Activar modo debug
?debug=assets#...

#### Ingresar a Aplicaciones
http://DOMAIN/web?debug=assets#view_type=kanban&model=ir.module.module&menu_id=45&action=31